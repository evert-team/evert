'use strict';
/**
 * Module dependencies.
 */
var init = require('./config/init')(),
	config = require('./config/config'),
	mongoose = require('mongoose'),
	log4js = require('log4js'),
	logger = log4js.getLogger('fileLogger');

/**
 * Main application entry file.
 * Please note that the order of loading is important.
 */

// Bootstrap db connection
try {
	var db = mongoose.connect(config.db, function(err) {
		if (err) {
			logger.error('\x1b[31m', 'Could not connect to MongoDB!');
			logger.error(err);
		}
	});
}
catch (e) {
	logger.error('Connection to data base lost, trying to reconnect');
	logger.error(e);
}

// Init the express application
var app = require('./config/express')(db);

// Bootstrap passport config
require('./config/passport')();

// Start the app by listening on <port>
app.listen(config.port);

// Expose app
exports = module.exports = app;

// Logging initialization
logger.info('MEAN.JS application started on port ' + config.port);
