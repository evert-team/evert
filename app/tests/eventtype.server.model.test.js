'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Eventtype = mongoose.model('Eventtype');

/**
 * Globals
 */
var user, eventtype, eventtype2;

/**
 * Unit tests
 */
describe('Eventtype Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});

		user.save(function() { 
			eventtype = new Eventtype({
				type : 'new',
				user : '54410970ffd7b2601c0d471d',
				id : '3',
				name : 'Book registration for course',
				isActive : '1',
				functions : [
					{
						name : 'student.number'
					}
				],
				buttons : [
					{
						value :'Date Now'
					},
					{
						name :'book.name',
						value :'Book Name'
					},
					{
						name : 'book.isbn',
						value : 'ISBN Number'
					},
					{
						name :'course.numberofstudents',
						value : 'Number of stundents'
					},
					{
						name : 'course.name',
						value : 'Course Name'
					},
					{
						name : 'course.stardate',
						value : 'Course startdate'
					},
					{
						name:'course.department',
						value:'Course Department'
					}
				]
			});

			eventtype2 = new Eventtype({
				type : 'new',
				user : '54410970ffd7b2601c0d471d',
				id : '3',
				name : 'Book registration for course',
				isActive : '1',
				functions : [
					{
						name : 'student.number'
					}
				],
				buttons : [
					{
						value :'Date Now'
					},
					{
						name :'book.name',
						value :'New Book Name'
					},
					{
						name : 'book.isbn',
						value : 'ISBN Number'
					},
					{
						name :'course.numberofstudents',
						value : 'Number of stundents'
					},
					{
						name : 'course.name',
						value : 'Course Name'
					},
					{
						name : 'course.stardate',
						value : 'Course startdate'
					},
					{
						name:'course.department',
						value:'Course Department'
					}
				]
			});

			done();
		});
	});

	describe('Method Save', function(){
		it('should be able to save without problems', function(done) {
			return eventtype.save(function(err) {
				should.not.exist(err);
				done();
			});
		});
		
		it('should update eventtype without problems', function(done) {
			eventtype.save();
			return eventtype2.save(function(err) {
				should.not.exist(err);
				done();
			});
		});
	});

	describe('Method Find', function() {
		it('should begin with no eventtypes', function(done) {
			Eventtype.find({}, function(err, eventtypes) {
				eventtypes.should.have.length(0);
				done();
			});
		});
	});

	describe('Method Remove', function() {
		it('should be able to remove an eventtype', function(done) {
			eventtype.save();
			return eventtype.remove(function(err) {
				should.not.exist(err);
				done();
			});
		});
	});

	afterEach(function(done) { 
		Eventtype.remove().exec();
		User.remove().exec();

		done();
	});

});