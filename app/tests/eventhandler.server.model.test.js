'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Eventhandler = mongoose.model('Eventhandler');
	// eventHandlerController = require('/home/gaialar/Dropbox/Verkefni/FinalProject/Solution/evert/app/controllers/eventhandlers');

/**
 * Globals
 */
var user, eventhandler, eventhandler2;

/**
 * Unit tests
 */
describe('Eventhandler Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});

		user.save(function() { 
			eventhandler = new Eventhandler({
				content: {
					reciver: '',
					message: '',
					subject: '',
					script: ''
				}, 
				parentId: 3,
				description: 'Send email to bookstore',
				name: 'email',
				__v: 0
			});

			eventhandler2 = new Eventhandler({
				content: {
					reciver: '',
					message: '',
					subject: '',
					script: ''
				},
				parentId: 3,
				description: 'Send email to students',
				name: 'email',
				__v: 0
			});

			done();
		});
/*
		eventHandlerController.incomingEvent(eventhandler, function(err) {
			//objectId.should.be.equal(3);
			done();
		});
*/
	});

/*
	describe('function incomingEvent', function() {
		it('should call export functions', function(done) {
			eventHandlerController.incomingEvent(eventhandler, function(err) {
				//objectId.should.be.equal(3);
				done();
			});
		});

		it('should be an event', function(eventObj){

		});
		it('should be able to call email handler', function(eventObj) {

		});
		it('should be able to call sms handler', function(eventObj) {
			//address = eventObj.
		});

	});

*/

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return eventhandler.save(function(err) {
				should.not.exist(err);
				done();
			});
		});

		it('should update eventhandler without problems', function(done) {
			eventhandler.save();
			return eventhandler2.save(function(err) {
				should.not.exist(err);
				done();
			});
		});
	});

	describe('Method Remove', function() {
		it('should update eventhandler without problems', function(done) {
			eventhandler.save();
			return eventhandler.remove(function(err) {
				should.not.exist(err);
				done();
			});
		});

		it('should be able to remove an eventhandler', function(done) {
			eventhandler.save();
			return eventhandler.remove(function(err) {
				should.not.exist(err);
				done();
			});
		});
	});

	describe('Method Find', function() {
		it('should begin with no eventhandlers', function(done) {
			Eventhandler.find({}, function(err, eventhandlers) {
				eventhandlers.should.have.length(0);
				done();
			});
		});
/*
		it('should be able to list an eventhandler using parentId', function(done) {
			var objectId = 3;
			eventhandler.save();
			Eventhandler.find({parentId: objectId}, function(err, eventhandlers) {
				eventhandlers.should.have.length(1);
				done();
			});
		});
*/
	});


	afterEach(function(done) { 
		Eventhandler.remove().exec();
		User.remove().exec();

		done();
	});
});