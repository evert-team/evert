'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var eventtypes = require('../../app/controllers/eventtypes');

	// Eventtypes Routes
	app.route('/eventtypes')
		.get(users.requiresLogin, eventtypes.list)
		.post(users.requiresLogin, eventtypes.create);

	app.route('/eventtypes/:eventtypeId')
		.get(eventtypes.read)
		.put(users.requiresLogin, eventtypes.hasAuthorization, eventtypes.update)
		.delete(users.requiresLogin, eventtypes.hasAuthorization, eventtypes.delete);

	// Finish by binding the Eventtype middleware
	app.param('eventtypeId', eventtypes.eventtypeByID);
};