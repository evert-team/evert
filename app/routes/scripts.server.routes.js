'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var scripts = require('../../app/controllers/scripts');

	// Scripts Routes
	app.route('/scripts')
		.get(scripts.list);

	app.route('/scripts/available')
		.get(scripts.availableList);

};