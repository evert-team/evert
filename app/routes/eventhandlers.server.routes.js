'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var eventhandlers = require('../../app/controllers/eventhandlers');

	// Eventhandlers Routes
	app.route('/eventhandler')
		.get(eventhandlers.list)
		.post(users.requiresLogin, eventhandlers.create);

	app.route('/eventhandler/:eventhandlerId')
		.get(eventhandlers.read)
		.put(users.requiresLogin, eventhandlers.hasAuthorization, eventhandlers.update)
		.delete(users.requiresLogin, eventhandlers.hasAuthorization, eventhandlers.delete);

	// Finish by binding the Eventhandler middleware
	app.param('eventhandlerId', eventhandlers.eventhandlerByID);
};