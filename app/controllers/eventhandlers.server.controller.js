'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors'),
    Eventhandler = mongoose.model('Eventhandler'),
    Handlebars = require('handlebars'),
    nodemailer = require('nodemailer'),
    eventMessage = require('./eventmessage'),
    config = require('../../config/config'),
    _ = require('lodash'),
    http = require('http'),
    api = {},
    client = {},
    log4js = require('log4js');

// Setting up logging in to file
log4js.loadAppender('file');
log4js.addAppender(log4js.appenders.file('logs/fileLogger.log'), 'fileLogger');

var logger = log4js.getLogger('fileLogger');

/**
 * Create a Eventhandler
 */
exports.create = function(req, res) {
    var eventhandler = new Eventhandler(req.body);
    eventhandler.user = req.user;
    eventhandler.save(function(err) {
        if (err) {
            logger.error('Could not create eventhandler ' + eventhandler.description + ' because of: ' + err);
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(eventhandler);
            logger.info('The event handler ' + eventhandler.description + ' has been created for event type ' + eventhandler.parentId);
        }
    });
};

/**
 * Show the current Eventhandler
 */
exports.read = function(req, res) {
    res.jsonp(req.eventhandler);
};

/**
 * Update a Eventhandler
 */
exports.update = function(req, res) {
    var eventhandler = req.eventhandler;
    eventhandler = _.extend(eventhandler, req.body);
    eventhandler.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(eventhandler);
        }
    });
};

/**
 * Delete an Eventhandler
 */
exports.delete = function(req, res) {
    var eventhandler = req.eventhandler;
    eventhandler.remove(function(err) {
        if (err) {
            logger.error(eventhandler.description + ' could not be deleted because of: ' + err);
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(eventhandler);
            logger.info('The event handler ' + eventhandler.description + ' has been deleted from event type ' + eventhandler.parentId);
        }
    });
};

/**
 * List of Eventhandlers
 */
exports.list = function(req, res) {
    Eventhandler.find({
        user: req.user._id
    }).populate('user', 'displayName').exec(function(err, eventhandlers) {
        if (err) {
            console.log('Im throwing an error');
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(eventhandlers);
        }
    });
};

/**
 * Eventhandler middleware
 */
exports.eventhandlerByID = function(req, res, next, id) {
    Eventhandler.find({
        _id: id,
        user: req.user._id
    }).populate('user', 'displayName').exec(function(err, eventhandler) {
        if (err) return next(err);
        if (!eventhandler) return next(new Error('Failed to load Eventhandler ' + id));
        req.eventhandler = eventhandler[0];
        next();
    });
};

/**
 * Eventhandler authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
    if (req.eventhandler.user.id !== req.user.id) {
        return res.status(403).send('User is not authorized');
    }
    next();
};

/**
 * What happens when an event is removed from the queue.
 */
var incomingEvent = exports.incomingEvent = function(eventData) {
    var objectId = eventData.typeId;
    // Get active handlers with matching parentId from database
    Eventhandler.find({
        parentId: objectId,
        active: true
    }, function(err, eventhandler) {
        if (err) return console.error(err);
        // For each handle call the appropriate handler.
        for (var i in eventhandler) {
            var address = eventhandler[i].content.reciver;
            var subject = eventhandler[i].content.subject;
            var mainContent = eventhandler[i].content.message;
            if (eventhandler[i].name === 'email') {
                var emailContent = exports.textAssembler(mainContent, eventData);
                exports.emailHandler(address, subject, emailContent);
            } else if (eventhandler[i].name === 'sms') {
                var smsContent = exports.textAssembler(mainContent, eventData);
                exports.smsHandler(address, smsContent, eventhandler[i]);
            } else if (eventhandler[i].name === 'js') {
                var scriptContent = exports.textAssembler(mainContent, eventData);
                exports.jsHandler(scriptContent, eventhandler[i]);
            }
        }
        eventMessage.deleteMessage();
    });
};

/**
 * Incoming event has JavaScript as a handler.
 */
exports.jsHandler = function(scriptContent, handler) {
    // If error in users script then log the error, needs to be logged to file
    try {
        eval(scriptContent);
        logger.info('The handler ' + handler.description + ' did run successfully.');
    } catch (e) {
        logger.warn('Could not run ' + handler.description + ' js handler because of an error: ' + e + '.');
    }
};

/*
 * Sending multiple messages
 */
exports.smsHandler = function(phoneNumber, smsContent, handler) {
    var numbers = phoneNumber.split(',');
    for (var i in numbers) {
        exports.sms(numbers[i], smsContent, handler);
    }
};

/*
 * Incoming event has an mobile message as a handler.
 */
exports.sms = function(phoneNumber, smsContent, handler, callback) {
    var user = config.sms.user,
        password = config.sms.pass,
        api_id = config.sms.apiID,
        contrycode = config.sms.countryCode,
        path = 'http://api.clickatell.com/http/sendmsg?user=' + user + '&password=' + password + '&api_id=' + api_id + '&to=' + contrycode + phoneNumber + '&text=' + smsContent;
    http.request(path).on('response', function(response) {
        var data = '';
        response.on('data', function(chunk) {
            data += chunk;
            console.log(data);
        });
        response.on('end', function() {
            if (data.toString().substring(0, 3) === 'ERR') {
                logger.error('Mobile message for ' + handler.description + ' was not sent see following error message: ' + data);
            } else {
                //callback(data);
                logger.info('Mobile message for ' + handler.description + ' was sent successfully.');
            }
        });
    }).end();
};

/**
 * Incoming event has an email as a handler.
 */
exports.emailHandler = function(emailAddress, emailSubject, emailContent) {
    // Create a SMTP transporter object.
    var smtpTransport = nodemailer.createTransport({
        service: config.mailer.options.service,
        auth: {
            user: config.mailer.options.auth.user,
            pass: config.mailer.options.auth.pass
        }
    });
    // Message object.
    var message = {
        from: config.mailer.from,
        to: emailAddress,
        subject: emailSubject,
        text: emailContent,
        headers: {
            'X-Laziness-level': 1000
        },
    };
    // Send message.
    smtpTransport.sendMail(message, function(error, info) {
        if (error) {
            logger.error('Error occurred');
            logger.error(error.message);
            return;
        }
        logger.info('Email message sent successfully!');
        logger.info('Server responded with "%s"', info.response);
    });
};

/**
 * Assembling text using handlebars.
 */
exports.textAssembler = function(content, data) {
    var handlebarTemplate = Handlebars.compile(content);
    var message = handlebarTemplate(data);
    return message;
};

/**
 * Calling client system api
 */
client.get = function(centrisApiPath, callback) {
    var options = {
        host: config.clientApi.host,
        path: '' + centrisApiPath + ''
    };
    http.request(options).on('response', function(response) {
        var data = '';
        response.on('data', function(chunk) {
            data += chunk;
        });
        response.on('end', function() {
            callback(JSON.parse(data));
        });
    }).end();
};

/**
 * Calling any restful api
 */
api.get = function(preferedPath, callback) {
    var options = {
        path: '' + preferedPath + ''
    };
    http.request(options).on('response', function(response) {
        var data = '';
        response.on('data', function(chunk) {
            data += chunk;
        });
        response.on('end', function() {
            callback(JSON.parse(data));
        });
    }).end();
};

/**
 * Wrapping email handler to be user friendly for user on front end
 */

var sendEmail = function(emailAddress, emailSubject, emailContent) {
    exports.emailHandler(emailAddress, emailSubject, emailContent);
};

/**
 * Wrapping sms handler to be user friendly for user on front end
 */
var sendSMS = function(phoneNumber, smsContent) {
    exports.smsHandler(phoneNumber, smsContent);
};
