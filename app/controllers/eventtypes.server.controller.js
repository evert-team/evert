'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors'),
    Eventtype = mongoose.model('Eventtype'),
    Eventhandler = mongoose.model('Eventhandler'),
    _ = require('lodash'),
    log4js = require('log4js'),
    logger = log4js.getLogger('fileLogger');

/**
 * Check if incoming eventtype already exists
 */
exports.doesExist = function(eventTypeData) {
    Eventtype.find({
        eventId: eventTypeData.id,
        user: eventTypeData.user
    }).populate('user', 'displayName').exec(function(err, eventtypes) {
        if (err) {
            logger.err('Im throwing an error');
        } else {
            if (eventtypes.length === 0) {
                exports.create(eventTypeData);
            } else {
                if (parseInt(eventTypeData.isActive, 10) === 0 && eventtypes[0].isActive) {
                    logger.warn('Requested to deactivate event type ' + eventTypeData.name + '.');
                    exports.update(eventtypes[0]);
                    exports.deactivateHandlers(eventTypeData.id);
                } else if (parseInt(eventTypeData.isActive, 10) === 1 && eventtypes[0].isActive === false) {
                    logger.warn('Requested to activate event type ' + eventTypeData.name + '.');
                    exports.update(eventtypes[0]);
                } else {
                    logger.warn('This event type does already exist.');
                }
            }
        }
    });
};

/**
 * Deactivate all handlers for specific event type
 */
exports.deactivateHandlers = function(id) {
    var old;
    // Get handlers with matching type Id from database
    Eventhandler.find({
        parentId: id,
        active: true
    }, function(err, eventhandlers) {
        if (err) return console.error(err);
        for (var i in eventhandlers) {
            old = eventhandlers[i];
            eventhandlers[i].active = !eventhandlers[i].active;
            eventhandlers[i] = _.extend(old, eventhandlers[i]);
            exports.saveUpdate(eventhandlers[i]);
        }
    });
};

/**
 * Saving updated handler
 */
exports.saveUpdate = function(eventhandler) {
    eventhandler.save(function(err) {
        if (err) {
            logger.error('Could not change status of activation: ' + err);
        } else {
            logger.info('Activation completed');
        }
    });
};

/**
 * Create an Eventtype
 */
exports.create = function(eventTypeData) {
    if (eventTypeData !== undefined) {
        var eventType = new Eventtype();
        eventType.eventId = eventTypeData.id;
        eventType.eventName = eventTypeData.name;
        eventType.comment = eventTypeData.comment;
        eventType.isActive = eventTypeData.isActive;
        eventType.user = eventTypeData.user;
        eventType.buttons = [];
        for (var button in eventTypeData.buttons) {
            eventType.buttons.push({
                name: eventTypeData.buttons[button].name,
                value: eventTypeData.buttons[button].value
            });
        }
        eventType.save(function(err) {
            if (err) {
                logger.error(err);
                logger.error('A problem has occurred while trying to save new event type to DB.');
            } else {
                logger.info('A new event type has been saved to Evert DB.');
            }
        });
    }
};

/**
 * Show the current Eventtype
 */
exports.read = function(req, res) {
    res.jsonp(req.eventtype);
};

/**
 * Update a Eventtype
 */
exports.update = function(eventtype) {
    var old = eventtype;
    eventtype.isActive = !eventtype.isActive;
    eventtype = _.extend(old, eventtype);
    eventtype.save(function(err) {
        if (err) {
            logger.error('Could not change status of activation: ' + err);
        } else {
            logger.info('Activation completed');
        }
    });
};

/**
 * Delete an Eventtype
 */
exports.delete = function(req, res) {
    var eventtype = req.eventtype;
    eventtype.remove(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(eventtype);
        }
    });
};

/**
 * List of Eventtypes from Evert
 */
exports.list = function(req, res) {
    Eventtype.find({
        user: req.user._id
    }).populate('user', 'displayName').exec(function(err, eventtypes) {
        if (err) {
            console.log('Im throwing an error');
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(eventtypes);
        }
    });
};

/**
 * Eventtype middleware
 */
exports.eventtypeByID = function(req, res, next, id) {
    Eventtype.find({
            eventId: id,
            user: req.user._id
        })
        .populate('user', 'displayName').exec(function(err, eventtype) {
            if (err) {
                console.log(err);
            }
            if (!eventtype) return next(new Error('Failed to load Eventtype ' + id));
            req.eventtype = eventtype[0];
            next();
        });
};

/**
 * Eventtype authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
    if (req.eventtype.user.id !== req.user.id) {
        return res.status(403).send('User is not authorized');
    }
    next();
};
