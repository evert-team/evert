'use strict';

/**
  * Module dependencies.
  */
var mongoose = require('mongoose'),
	errorHandler = require('./errors'),
	Script = mongoose.model('Script'),
	_ = require('lodash'),
	log4js = require('log4js'),
	logger = log4js.getLogger('fileLogger');

/**
  * Create a Script
  */
var create = function(scriptData) {
	var script = new Script();
	script.user = scriptData.user;
	for (var i in scriptData.functions) {
		script.functions.push({
			name: scriptData.functions[i].name,
			value: scriptData.functions[i].value,
			comment: scriptData.functions[i].comment
		});
	}
		
	script.save(function(err) {
		if (err) {
			logger.error(err);
			logger.error('A problem has occurred while trying to save new functions.');
		} else {
			logger.info('A new function has been saved to Evert DB.');
		}
	});
};

/**
  * Update a Script
  */
var update = function(scriptData, scripts) {
	
	if(scripts.length > 1) {
		logger.warn('There should only be one');
	}

	var script = scripts[0];

	for (var i in scriptData.functions) {
		script.functions.push({
			name: scriptData.functions[i].name,
			value: scriptData.functions[i].value,
			comment: scriptData.functions[i].comment
		});
	}
	script.save(function(err) {
		if (err) {
			logger.error(err);
			logger.error('A problem has occurred while trying to add functions.');
		} else {
			logger.info('A new function has been added to Evert DB.');
		}
	});
};

/**
  * List of Scripts
  */
exports.availableList = function(data) {
	Script.find({user: data.user}).sort('-created').populate('user', 'displayName').exec(function(err, scripts) {
		if (err) {
			console.log(err);
		} else {
			if (scripts[0] === undefined) {
				create(data);
			}
			else {
				update(data, scripts);
			}
		}
	});
};

/**
  * List of available scripts from client
  */
exports.list = function(req, res) {
	Script.find({user: req.user._id}).sort('-created').populate('user', 'displayName').exec(function(err, scripts) {
		if (err) {
			console.log('Im throwing an error');
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(scripts);
		}
	});
};

/**
  * Script authorization middleware
  */
exports.hasAuthorization = function(req, res, next) {
	if (req.script.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};