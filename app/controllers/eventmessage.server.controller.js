'use strict';
var mongoose = require('mongoose'),
    errorHandler = require('./errors'),
    EventMessage = mongoose.model('Eventmessage'),
    _ = require('lodash'),
    newMessage = {},
    amqp = require('amqplib'), // Library to talk to Rabbit.
    when = require('when'), // Promise library.
    eventHandler = require('./eventhandlers'),
    eventTypes = require('./eventtypes'),
    scripts = require('./scripts'),
    config = require('../../config/config'),
    log4js = require('log4js'),
    logger = log4js.getLogger('fileLogger');

/**
 * Checking if message is a new eventtype or a new event.
 */
exports.checkingMessage = function() {
    if (newMessage.string.type === 'event') {
        // The message is an event
        eventHandler.incomingEvent(newMessage.string);
        logger.info('Event with id ' + newMessage.string.id + ' arrived');
    } else if (newMessage.string.type === 'new') {
        // The message is a new event type
        eventTypes.doesExist(newMessage.string);
        logger.info('New event type has arrived: ');
        logger.info('Id: ' + newMessage.string.typeId + ' ' + newMessage.string.name);
    } else if (newMessage.string.type === 'functions') {
        // The message contains available function to call clients API
        scripts.availableList(newMessage.string);
    } else {
        logger.warn('Evert has problems reading this json file, check formatting of json file.');
    }
};

/**
 * Deleting the message from db once we processed all the handlers.
 */
exports.deleteMessage = function() {
    EventMessage.findOne({
        _id: newMessage.id
    }, function(err, query) {
        query.remove();
        logger.info('Evert has deleted the message string in DB.');
    });
};

/**
 * Get the message, process it and apply it to a variable.
 */
exports.getMessage = function() {
    var query = EventMessage.find().limit(1).sort({
        $natural: -1
    });
    query.exec(function(err, message) {
        if (err) {
            logger.error('Evert has some problems querying the message string in DB.');
        } else {
            newMessage.string = JSON.parse(message[0].script);
            newMessage.id = message[0]._id;
            logger.info('Evert has assigned the incoming string from DB to an object.');
            exports.checkingMessage();
        }
    });
};

/**
 * Inserts the event message into DB
 */
exports.postMessage = function(msg) {
    var message = new EventMessage();
    message.script = msg;
    message.save(function(err) {
        if (err) {
            logger.error('Evert has some problems saving the message string in DB');
        } else {
            logger.info('Evert has has successfully saved the message string in DB');
            exports.getMessage();
        }
    });
};

/**
 * Getting string from Mongodb that is form RabbitMQ evert queue.
 * Starting Rabbit when Evert starts.
 * We need to change localhost when Rabbit will be running on a server.
 */
amqp.connect(config.rabbit.url).then(function(conn) {
    process.once('SIGINT', function() {
        conn.close();
    });
    return conn.createChannel().then(function(ch) {
        // Listen for a queue named Evert.
        var ok = ch.assertQueue(config.rabbit.queue, {
            durable: false
        });
        // Fetch message from queue.
        ok = ok.then(function(_qok) {
            return ch.consume(config.rabbit.queue, function(msg) {
                // Call a function that saves message to db.
                exports.postMessage(msg.content.toString());
            }, {
                noAck: true
            });
        });
        return ok.then(function(_consumeOk) {
            //log that Rabbit is running.
            logger.info('Evert RabbitMQ listener is up and running and is waiting for messages...');
            logger.warn('Warning Evert is using eval() which is a dangerous function, which executes the code it´s passed with the privileges of the caller.');
        });
    });
}).then(null, console.warn);
