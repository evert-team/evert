'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Eventhandler Schema
 */
var EventhandlerSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Eventhandler name',
		trim: true
	},
	description: {
		type: String,
		required: 'Please add description',
		default: ''
	},
	parentId: {
		type: Number,
		default: ''
	},
	active: {
		type: Boolean,
		default: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	content: {
		subject: {
			type: String,
			default: ''
		},
		message: {
			type: String,
			default: ''
		},
		reciver: {
			type: String,
			default: ''
		}
	},
});

mongoose.model('Eventhandler', EventhandlerSchema);