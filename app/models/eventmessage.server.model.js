'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;
 
var EventmessageSchema = new Schema({
	script: {
		type: String,
		default: '',
		required: 'Please fill Eventmessage name',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Eventmessage', EventmessageSchema);