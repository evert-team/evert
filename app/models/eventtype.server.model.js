'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Eventtype Schema
 */
var EventtypeSchema = new Schema({
	time : { 
		type : Date, default: Date.now 
	},
	eventId: {
		type: Number,
	},
	comment: {
		type: String,
		default: '',
	},
	isActive: {
		type: Boolean,
		default: true
	},
	eventName: {
		type: String,
		default: '',
		trim: true
	},
	buttons: [
		{
			name: {
				type: String
			},
			value: {
				type: String
			}
		}
	],
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Eventtype', EventtypeSchema);