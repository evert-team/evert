'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Script Schema
 */
var ScriptSchema = new Schema({
	functions: [
		{
			name: {
				type: String,
				default: ''
			},
			value: {
				type: String,
				default: ''
			},
			comment: {
				type: String,
				default: ''
			}
		}
	],
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Script', ScriptSchema);