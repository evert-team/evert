[![Evert Logo](https://dl.dropboxusercontent.com/u/95180362/evert_logo_small.png)](https://dl.dropboxusercontent.com/u/95180362/evert_logo_small.png)

Evert is a system that makes event handling accessible in big systems. It provides a quick and user-friendly approach to event handling with the possibility of a more detailed handling for those who have programming skills. The system is built on MEAN.JS which is a full-stack JavaScript open-source solution, which provides a solid starting point for [MongoDB](http://www.mongodb.org/), [Node.js](http://www.nodejs.org/), [Express](http://expressjs.com/), and [AngularJS](http://angularjs.org/) based applications. 


## Prerequisites 
Make sure you have installed all these prerequisites on your development machine.
* Node.js - [Download & Install Node.js](http://www.nodejs.org/download/) and the npm package manager, if you encounter any problems, you can also use this [Github Gist](https://gist.github.com/isaacs/579814) to install Node.js.
* MongoDB - [Download & Install MongoDB](http://www.mongodb.org/downloads), and make sure it's running on the default port (27017).
* Bower - You're going to use the [Bower Package Manager](http://bower.io/) to manage your front-end packages, in order to install it make sure you've installed Node.js and npm, then install bower globally using npm:

```
$ npm install -g bower
```

* Grunt - You're going to use the [Grunt Task Runner](http://gruntjs.com/) to automate your development process, in order to install it make sure you've installed Node.js and npm, then install grunt globally using npm:

```
$ sudo npm install -g grunt-cli
```

## Downloading Evert 

### Cloning The Bitbucket Repository

```
$ git clone git@bitbucket.org:evert-team/evert.git
```
This will clone the latest version of the Evert repository to an **evert** folder.

## Quick Install
Once you've downloaded the project and installed all the prerequisites, you're just a few steps away from starting your Evert application.

The first thing you should do is install the Node.js dependencies. The project comes pre-bundled with a package.json file that contains the list of modules you need to start your application, to learn more about the modules installed visit the NPM & Package.json section.

To install Node.js dependencies you're going to use npm again, in the application folder run this in the command-line:

```
$ npm install
```

This command does a few things:
* First it will install the dependencies needed for the application to run.
* If you're running in a development environment, it will then also install development dependencies needed for testing and running your application.
* Finally, when the install process is over, npm will initiate a bower installcommand to install all the front-end modules needed for the application

## Running Your Application
After the install process is over, you'll be able to run your application using Grunt, just run grunt default task:

```
$ grunt
```

Your application should run on the 3000 port so in your browser just go to [http://localhost:3000](http://localhost:3000)

## Setting up a connection to your API

Before you start using your newly installed Evert system you will need to do three things. Adapt the config file, sett up an connection through RabbitMQ and set up Json strings to send through RabbitMQ.

### Adapting the Config File

Before running Evert with your system you will have to change a few things in config files. There are four main config files to keep in mind, all.js, development.js, production.js and test.js. They are all located under the config/env folder. 

To set your system up for production you will only have to set up production.js and all.js but if you plan on further development it would be recomended to set up the development.js and test.js files as well. All references to changes in production.js should apply to the other two files, the setup is identical except in the case of database connections in .

#### RabbitMQ configuration
After setting up a RabbitMQ server you should change the configuration in the all.js file so Evert will listen to your queue. You will have to set the correct connection string. 

#### Email configuration
To set Evert to send mail from your mailing system you will have to change the mailer section in production.js to comply with your email host.

#### Text messages configuration
To set Evert to send text messages you will have to change the textMessage section in production.js to comply with the company you have a contract with.

#### Connection to your API
To set Evert to interact with your API you will have to set the connection string in the clientAPI section in production.js.

### Setting up RabbitMQ

#### Installation
This project is set up to listen to RabbitMQ. To use the queue you will have to download and set up the correct standalone package for your OS from [http://www.rabbitmq.com/download.html](http://www.rabbitmq.com/download.html).

#### Setup
After installation you will need to run the server on your system.
##### Windows
The RabbitMQ server starts automatically. You can stop/reinstall/start the RabbitMQ service from the Start Menu.
##### Mac OS x
Invoke the shell script:
```
$ sbin/rabbitmq-server
```
This displays a short banner message, concluding with the message "completed with [n] pligins.", indicationg that the RabbitMQ broker has been started successfully.
##### Ubuntu/Debian
Start and stop the server as usual for Debian using:
```
$ invoke-rc.d rabbitmq-server stop/start/etc.
```

#### npm install
Setting up a connection from Evert to RabbitMQ is done by installing the amqplib and when libraries through npm install, these will be included in your package.json file that comes with evert.
```
$ npm install
```

#### Config
Refer to the configuration section for information about RabbitMQ setup within Evert.

#### Using RabbitMQ
Now you are all set up and the next step is to start sending messages through RabbitMQ from your API to Evert. See RabbitMQ official documentation page for further information [http://www.rabbitmq.com/documentation.html](http://www.rabbitmq.com/documentation.html).

### Formatting Your Json Strings

There are three types of Json strings acceptable by evert. Event type, event message and function. They are signified at the start of a Json string by setting the value of type. All first level items are necessary, they need to be there and in most cases there needs to be a valid value added to them. The sublevel items are in all cases optional. The properties for buttons and placeholders can be as few or as many as is fitting for any event type. However the number of items and naming conventions should always match between an event type and a corresponding event message.

#### Event Type
Evert will only listen for and handle event messages regarding already known event types. Here below you can see a sample of an event type.

```
{
	"type":"new",
	"user":"544170f6da8b5ad0829f3637",
	"id":"3",
	"name":"Book registration for course",
	"comment":"1 is for an active event 0 is to deactivate the event.",
	"isActive":"1",
	"buttons":[
		{
			"value":"Date Now"
		},
		{
			"name":"book.name",
			"value":"Book Name"
		},
		{
			"name":"book.isbn",
			"value":"ISBN Number"
		},
		{
			"name":"course.numberofstudents",
			"value":"Number of stundents"
		},
		{
			"name":"course.name",
			"value":"Course Name"
		},
		{
			"name":"course.stardate",
			"value":"Course startdate"
		},
		{
			"name":"course.department",
			"value":"Course Department"
		}
	]
}
```

To signify that the message contains an event type set type to new. All access to event types is user restricted and thus the next item is to set the user to a correct users id. Setting the isActive flag to 1 signifies that Evert should be expecting events of that type and should handle them when they come in. Setting the isActive flag to 0 signifies that Evert should not handle events of that type even if they come through. If an event type is deactivated only type, user, id and isActive need to be set and available in the Json object.
After that the only thing left is to set name and value of data that vill be available in incoming events.

#### Event Message

When Evert has gotten information over RabbitMQ on an Event type the next thing on your list should be to create a template for events of that type that occurr within your system. 

```
{
	"type":"event",
	"typeId":"3",
	"date":"20/09/2014",
	"comment":"This should have dd/mm/yyyy as a format",
	"book":{
		"name":"Problem Solving with C++",
		"isbn":"978-0132162739"
	},
	"course":{
		"numberofstudents":"378",
		"name":"Forritun",
		"instanceID":"24761",
		"startdate":"18/08/2014",
		"department":"Tölvunarfræðideild"
	}
}
```

The type should be set as "event" and the typeId should mach the id of the corresponding event type and the information should correspond with the names set in the event type. If the buttons are compared to the information in the event message you will see that the naming maches up with the two subobjects in the event message Json string.

#### Function

If you want to set up an accessible way for users to write scripts with functions from your system the following message should be sent through the rabbitMQ.

```
{
	"type":"functions",
	"user":"54410970ffd7b2601c0d471d",
	"functions":[
		{
			"name":"Students in a Course",
			"value":"Here you would insert information about your own function",
			"comment":"Special call to get all students in a given course, in each object is groupId, ssn, full name, grade, teachers comment. This is to list all students and information only to view for teachers."
		}
	]
}
```

The type should be set to "functions", the users id has to be set to the correct user and then the functions should follow in an array. Each function has to have a name, value and a comment. The name would provide a descriptive caption for your function, the value would contain the connection string for your function and the comment would give the user more information by hoveing over the function in a list of functions from your API that will be available in Evert.

## And you are done!  

That's it! your application should be running by now, if you plan on continuing development please refer to the MEAN.JS documentation and refer to the Configuration chapter for suggestions on setup while developing.

## In Case You Want to Continue Development 
Before you begin we recommend you read about the basic building blocks that assemble a MEAN.JS application: 
* MongoDB - Go through [MongoDB Official Website](http://mongodb.org/) and proceed to their [Official Manual](http://docs.mongodb.org/manual/), which should help you understand NoSQL and MongoDB better.
* Express - The best way to understand express is through its [Official Website](http://expressjs.com/), particularly [The Express Guide](http://expressjs.com/guide.html); you can also go through this [StackOverflow Thread](http://stackoverflow.com/questions/8144214/learning-express-for-node-js) for more resources.
* AngularJS - Angular's [Official Website](http://angularjs.org/) is a great starting point. You can also use [Thinkster Popular Guide](http://www.thinkster.io/), and the [Egghead Videos](https://egghead.io/).
* Node.js - Start by going through [Node.js Official Website](http://nodejs.org/) and this [StackOverflow Thread](http://stackoverflow.com/questions/2353818/how-do-i-get-started-with-node-js), which should get you going with the Node.js platform in no time.

## Credits
Inspired by the great work of [Madhusudhan Srinivasa](https://github.com/madhums/)
The MEAN name was coined by [Valeri Karpov](http://blog.mongodb.org/post/49262866911/the-mean-stack-mongodb-expressjs-angularjs-and)

## License
(The MIT License)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.