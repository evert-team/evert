'use strict';

//Setting up route
angular.module('scripts').config(['$stateProvider',
	function($stateProvider) {
		// Scripts state routing
		$stateProvider.
		state('listScripts', {
			url: '/scripts',
		}).
		state('createScript', {
			url: '/scripts/create',
		}).
		state('viewScript', {
			url: '/scripts/:scriptId',
		}).
		state('editScript', {
			url: '/scripts/:scriptId/edit',
		});
	}
]);