'use strict';

// Scripts controller
angular.module('scripts').controller('ScriptsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Scripts',
	function($scope, $stateParams, $location, Authentication, Scripts ) {
		$scope.authentication = Authentication;

		// Find a list of Scripts
		$scope.find = function() {
			$scope.scripts = Scripts.query();
		};
	}
]);