'use strict';

angular.module('eventhandlers').controller('DeleteAllController', ['$scope', '$modalInstance', 'eventhandlers',
	function ($scope, $modalInstance, eventhandlers) {

		$scope.ok = function () {
			$modalInstance.close(eventhandlers);
		};

		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}
]);
