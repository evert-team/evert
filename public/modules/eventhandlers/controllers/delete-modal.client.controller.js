'use strict';

angular.module('eventhandlers').controller('ModalInstanceCtrl', function ($scope, $modalInstance, handler) {
	
	$scope.handler = handler.description;
	
	$scope.ok = function () {
		$modalInstance.close(handler);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});