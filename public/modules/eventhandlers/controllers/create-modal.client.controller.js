'use strict';

angular.module('eventhandlers').controller('CreateModalController', ['$scope', '$modalInstance', 'description',
	function($scope, $modalInstance, description) {
		
		$scope.type = description;
		$scope.ok = function (data) {
			$modalInstance.close(data);
		};

		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}
]);