'use strict';

// Eventhandlers controller
angular.module('eventhandlers').
controller('EventhandlersController', ['$scope', '$rootScope', '$stateParams', '$location', 'Authentication', 'Eventhandlers', '$modal', '$log', '$state', '$timeout', '$http',
    function($scope, $rootScope, $stateParams, $location, Authentication, Eventhandlers, $modal, $log, $state, $timeout, $http) {
        $scope.authentication = Authentication;

        var Range = ace.require('ace/range').Range,
            example = [
                '//Example of using others systems API to get',
                '//further information to use in your own script,',
                '//for example in emails, text messages, etc.',
                '//This example uses the information to send an email',
                '/*',
                ' * //Set the email address and subject to your liking.',
                ' * var address = "Add email address";',
                ' * var subject = "Add subject;"',
                ' *',
                ' * //Get information from Centris, the client API.',
                ' * //This information is only available within,',
                ' * //the following function.',
                ' * client.get("/api/api/v1/courses/?department=1", function (data) {',
                ' *    for ( var i in data) {',
                ' *        if( data[i].Name === "{{course.name}}") {',
                ' *            var mainContent = "The book {{book.name}} has been registered',
                ' *            to {{course.name}} in {{department.name}}.',
                ' *            There are  " + data[i].RegisteredStudents +',
                ' *            " students in this course this semester";',
                ' *            sendEmail(address, subject, mainContent);',
                ' *        }',
                ' *    }',
                ' * });',
                ' */'
            ].join('\n');

        $scope.placeholder = {};
        $scope.client = {};
        $scope.evert = {};
        $scope.query = {};
        $scope.isDirty = false;

        // Adding information from selected button to template
        $scope.addInfo = function(data, type, functionality) {

            if (type === 'info') {

                $scope.infoToAdd = '{{' + data + '}}';
            } else if (type === 'client') {

                $scope.infoToAdd = [
                    'client.get("' + data + '", function (data) {',
                    '    console.log(data);',
                    '});',
                ].join('\n');
            } else if (type === 'evert') {
                $scope.infoToAdd = data;
            } else if (type === 'api') {
                $scope.infoToAdd = [
                    'var path = "Add path"',
                    'api.get(path, function(data) {',
                    '    console.log(data);',
                    '});'
                ].join('\n');
            }

            // Selecting to which editor information is added  
            if (functionality === 'js') {
                $scope.addAceInfo($scope.infoToAdd);
            } else {
                $rootScope.$broadcast('addInfo', $scope.infoToAdd, $scope.eventhandler);
            }
        };

        // Create new Eventhandler
        $scope.create = function(type, description, id) {
            
            // Create new Eventhandler object
            var eventhandler = new Eventhandlers({
                name: type,
                parentId: $stateParams.eventtypeId,
                description: description,
                content: {
                    message: ''
                }
            });

            if (type === 'js') {
                eventhandler.content.message = example;
            }

            // Redirect after save
            eventhandler.$save(function(response) {
                $location.path('eventtype/' + response.parentId + '/eventhandler/' + response._id);

                $scope.eventhandlers.push(response);

                // Clear form fields
                $scope.name = '';
                $scope.description = '';
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Eventhandler
        var remove = function(eventhandler) {

            // Change view if viewing deleted handler
            if (eventhandler._id === $stateParams.eventhandlerId) {
                $location.path('eventtype/' + eventhandler.parentId);
            }

            // Remove from handler list
            if (eventhandler) {
                eventhandler.$remove();
                for (var i in $scope.eventhandlers) {
                    if ($scope.eventhandlers[i] === eventhandler) {
                        $scope.eventhandlers.splice(i, 1);
                    }
                }
            } else {
                $scope.eventhandler.$remove(function() {
                    $location.path('eventhandlers');
                });
            }
        };

        // Deleting all available eventhandlers for this type
        var removeAll = function() {
            var i = 0;
            var type = $stateParams.eventtypeId;
            for (i; i < $scope.eventhandlers.length; i++) {
                if (parseInt($scope.eventhandlers[i].parentId, 10) === parseInt(type, 10)) {
                    remove($scope.eventhandlers[i]);
                    i--;
                }
            }
        };

        // When isDirty set to true eventhandlers list is updated
        $scope.change = function() {
            $scope.isDirty = true;
        };

        // Update existing Eventhandler
        $scope.update = function(response, putResponseHeaders) {

            var eventhandler = $scope.eventhandler;
            var id = $stateParams.eventtypeId;
            eventhandler.$update(function(response) {
                response.$promise.then(function() {
                    $scope.saved = true;
                    $timeout(function() {
                        $scope.saved = false;
                    }, 2000);

                    if ($scope.isDirty) {
                        $state.reload('eventtype.viewEventtype');
                    }
                    $scope.isDirty = false;
                });

                $location.path('eventtype/' + id + '/eventhandler/' + eventhandler._id);

            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of Eventhandlers
        $scope.find = function() {
            $scope.showAll = true;
            $scope.id = parseInt($stateParams.eventtypeId, 10);

            var eventhandlers = Eventhandlers.query();

            eventhandlers.$promise.then(function(handler) {
                $scope.eventhandlers = handler;
            });
        };

        // Find existing Eventhandler
        $scope.findOne = function() {
            $scope.eventhandler = Eventhandlers.get({
                eventhandlerId: $stateParams.eventhandlerId,
            });
        };

        // Confirmation modal for deleting  handling
        $scope.deleteConfirm = function(handlers) {
            $scope.handlers = handlers;
            var modalInstance = $modal.open({
                templateUrl: 'delete.html',
                controller: 'ModalInstanceCtrl',
                resolve: {
                    handler: function() {
                        return handlers;
                    }
                }
            });

            modalInstance.result.then(function(handler) {
                remove(handler);
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        // Confirmation modal for deleting all handlings for this type
        $scope.deleteAllConfirm = function() {
            var modalInstance = $modal.open({
                templateUrl: 'deleteAll.html',
                controller: 'DeleteAllController',
                resolve: {
                    eventhandlers: function() {
                        return $scope.eventhandlers;
                    }
                }
            });

            modalInstance.result.then(function(eventhandlers) {
                removeAll(eventhandlers);
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        // Create modal to create new handling
        $scope.createHandler = function(type) {

            var createModal = $modal.open({
                templateUrl: 'createModal.html',
                controller: 'CreateModalController',
                resolve: {
                    description: function() {
                        return type;
                    }
                }
            });

            createModal.result.then(function(description) {
                $scope.create(type, description);
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        //Available evert functions
        $scope.evertFunctions = function() {

            $scope.functions = [{
                name: 'Email',
                value: [
                    'var address = "Add email address";',
                    'var subject = "Add subject;"',
                    'var mainContent = "Add content";',
                    'sendEmail(address, subject, mainContent);'
                ].join('\n')
            }, {
                name: 'Text message',
                value: [
                    'var phoneNumber = "Add phone number";',
                    'var mainContent = "Add content";',
                    'sendSMS(phoneNumber, mainContent);'
                ].join('\n')
            }];
        };

        // ACE editor
        $scope.aceLoaded = function(_editor) {
            // Editor part
            var _session = _editor.getSession();
            var _renderer = _editor.renderer;
            $scope.editor = _editor;

            // Options
            _renderer.setShowGutter(false);

            // Listens for change on editor
            _session.on('change', function() {
                var position = _editor.selection.getCursor();
            });
        };

        // Adding information to ACE editor
        $scope.addAceInfo = function(handler) {

            // Finding position of cursor
            var pos = $scope.editor.selection.getCursor();

            // Insert placeholder
            $scope.editor.getSession().insert(pos, handler);
            $scope.eventhandler.content.message = $scope.editor.getSession().getValue();

        };
    }
]);
