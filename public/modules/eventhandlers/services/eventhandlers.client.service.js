'use strict';

//Eventhandlers service used to communicate Eventhandlers REST endpoints
angular.module('eventhandlers').factory('Eventhandlers', ['$resource',
	function($resource) {
		return $resource('eventhandler/:eventhandlerId', { eventhandlerId: '@_id'
			}, {
				update: {
					method: 'PUT'
				}
			});
	}
]);