'use strict';

(function() {
	describe('Testing Eventhandlers Controller', function() {
		// Initialize global variables
		var EventhandlersController,
		scope,
		$httpBackend,
		$stateParams,
		$location;


		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Eventhandlers controller.
			EventhandlersController = $controller('EventhandlersController', {
				$scope: scope
			});
		}));

		describe('returns correct information', function() {
			it('placeholder information added', function() {
				var type = 'info';
				var data = 'book.name';
				var result = '{{' + data + '}}';
				scope.addInfo(data, type);

				expect(result).toEqual(scope.infoToAdd);
			});
			it('client information added', function() {
				var type = 'client';
				var data = '/api/api/v1/courses';
				var result = [
                        'client.get("'+ data +'", function (data) {',
                        '    console.log(data);',
                        '});'
                    ].join('\n');
				
				scope.addInfo(data, type);

				expect(result).toEqual(scope.infoToAdd);
			});

		});
	/*
		it('$scope.find() should create an array with at least one Eventhandler object fetched from XHR', inject(function(Eventhandlers) {
			// Create sample Eventhandler using the Eventhandlers service
			var email = new Eventhandlers({
				name: 'Send email to students'
			});

			var sms = new Eventhandlers({
				name: 'Send sms to students'
			});

			// Create a sample Eventhandlers array that includes the new Eventhandler
			var eventhandlers = [email, sms];

			// Set GET response
			$httpBackend.expectGET(/eventhandler).respond(eventhandlers);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.eventhandlers).toEqualData(eventhandlers);
		}));

		it('$scope.findOne() should create an array with one Eventhandler object fetched from XHR using a eventhandlerId URL parameter', inject(function(Eventhandlers) {
			// Define a sample Eventhandler object
			var sampleEventhandler = new Eventhandlers({
				name: 'New Eventhandler'
			});

			// Set the URL parameter
			$stateParams.eventhandlerId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/eventhandlers\/([0-9a-fA-F]{24})$/).respond(sampleEventhandler);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.eventhandler).toEqualData(sampleEventhandler);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Eventhandlers) {
			
			$stateParams.eventtypeId = 2;

			// Create a sample Eventhandler object
			var sampleEventhandlerPostData = new Eventhandlers({
				name: 'mail',
                parentId: $stateParams.eventtypeId,
                description: 'Send email to students',
                content: {
					message:''
				}
			});

			// Create a sample Eventhandler response
			var sampleEventhandlerResponse = new Eventhandlers({
				_id: '525cf20451979dea2c000001',
				name: 'mail',
				parentId : '2',
				description : 'Send email to students',
			});
			// Fixture mock form input values
			scope.name = 'mail';

			// Set POST response
			$httpBackend.expectPOST('eventhandler', sampleEventhandlerPostData).respond(sampleEventhandlerResponse);

			// Run controller functionality
			scope.create('mail', 'Send email to students');
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Eventhandler was created
			expect($location.path()).toBe('/eventtype/2/eventhandler/525cf20451979dea2c000001' );
		}));

		it('$scope.update() should update a valid Eventhandler', inject(function(Eventhandlers) {
			// Define a sample Eventhandler put data
			var sampleEventhandlerPutData = new Eventhandlers({
				_id: '525cf20451979dea2c000001',
				name: 'New Eventhandler'
			});

			// Mock Eventhandler in scope
			scope.eventhandler = sampleEventhandlerPutData;

			// Set PUT response
			$httpBackend.expectPUT(/eventhandlers\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/eventhandlers/' + sampleEventhandlerPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid eventhandlerId and remove the Eventhandler from the scope', inject(function(Eventhandlers) {
			// Create new Eventhandler object
			var sampleEventhandler = new Eventhandlers({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Eventhandlers array and include the Eventhandler
			scope.eventhandlers = [sampleEventhandler];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/eventhandler\/([0-9a-fA-F]{24})$/).respond(204);
			$httpBackend.expect
			// Run controller functionality
			scope.remove(sampleEventhandler);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.eventhandlers.length).toBe(0);
		}));
		
		it('$scope.removeAll() should send a DELETE request with a valid eventhandlerId and remove the Eventhandler from the scope', inject(function(Eventhandlers) {
			// Create new Eventhandler object
			var sampleEventhandler = new Eventhandlers({
				_id: '525a8422f6d0f87f0e407a33'
			});

			var sampleEventhandlerTwo = new Eventhandlers({
				_id: '525a8422f6d0f87f0e407a34'
			});

			// Create new Eventhandlers array and include the Eventhandler
			scope.eventhandlers = [sampleEventhandler, sampleEventhandlerTwo];

	
			// Run controller functionality
			scope.removeAll();
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.eventhandlers.length).toBe(0);
		}));*/
	});
}());