'use strict';

//Setting up route
angular.module('eventhandlers').config(['$stateProvider',
	function($stateProvider) {
		//Eventhandlers state routing
		$stateProvider.
		state('eventtype.viewEventtype.handler', {
			url: '/eventhandler/:eventhandlerId',
			templateUrl: 'modules/eventhandlers/views/view-eventhandler.client.view.html'
		});
	}
]);