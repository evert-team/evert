'use strict';

angular.module('eventtypes').directive('information', [ '$rootScope',  function( $rootScope) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				$rootScope.$on('addInfo', function(e, val, handler) {

					var template = element[0];
					var startPos = template.selectionStart;
					var endPos = template.selectionEnd;
					var scrollTop = template.scrollTop;

					template.value = template.value.substring(0, startPos) + val + template.value.substring(endPos, template.value.length);
					template.focus();
					template.selectionStart = startPos + val.length;
					template.selectionEnd = startPos + val.length;
					template.scrollTop = scrollTop;

					handler.content.message = template.value;
				});
			}
		};
	}
]);


