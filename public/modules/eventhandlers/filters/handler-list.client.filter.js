'use strict';

angular.module('eventhandlers').filter('handlerList', [
	function() {
		return function(handlers, id, isActive) {
			var result = [],
				i = 0;
			// If checkbox checked then filter out disabled handlers
			if (isActive) {
				for (i in handlers) {
					if (handlers[i].active === true && handlers[i].parentId === id) {
						result.push(handlers[i]);
					}
				}
			} else {
				for (i in handlers) {
					if (handlers[i].parentId === id) {
						result.push(handlers[i]);
					}
				}
			}
			return result;
		};
	}
]);

