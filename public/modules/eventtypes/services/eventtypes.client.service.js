'use strict';

//Eventtypes service used to communicate Eventtypes REST endpoints
angular.module('eventtypes').factory('Eventtypes', ['$resource',
	function($resource) {
		return $resource('eventtypes/:eventtypeId', { eventtypeId: '@eventId'
		},  {
			update: {
				method: 'PUT'
			}
		});
	}
]);