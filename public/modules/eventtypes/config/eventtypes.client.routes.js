'use strict';

//Setting up route
angular.module('eventtypes').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Eventtypes state routing
		$stateProvider.
		state('eventtype', {
			url: '/eventtype',
			templateUrl: '/modules/eventtypes/views/eventtype.client.view.html'
		}).
		state('eventtype.viewEventtype', {
			url: '/:eventtypeId',
			views: {
				'name' : {
					templateUrl: 'modules/eventtypes/views/name-eventtype.client.view.html'
				},
				'handlerView':{
					templateUrl: 'modules/eventhandlers/views/eventhandler.client.view.html'
				},
				'handlerList': {
					url:'',
					templateUrl: 'modules/eventhandlers/views/list-eventhandlers.client.view.html'
				}
			}
		}).
		state('eventtype.viewEventtype.handler.buttons', {
			url:'',
			templateUrl: 'modules/eventtypes/views/view-eventtype.client.view.html'
		}).
		state('listEventtypes', {
			url: '/eventtypes',
			templateUrl: 'modules/eventtypes/views/list-eventtypes.client.view.html'
		});
	}
]);