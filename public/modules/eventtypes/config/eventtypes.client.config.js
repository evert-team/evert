'use strict';

// Configuring the Articles module
angular.module('eventtypes').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Eventtypes', 'eventtypes', 'dropdown');
		Menus.addSubMenuItem('topbar', 'eventtypes', 'List Eventtypes', 'eventtypes');
		Menus.addSubMenuItem('topbar', 'eventtypes', 'New Eventtype', 'eventtypes/create');
	}
]);