'use strict';

(function() {
	describe('Testing Eventtypes Controller', function() {
		// Initialize global variables
		var EventtypesController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// Inject a service and attach it to a variable
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Eventtypes controller.
			EventtypesController = $controller('EventtypesController', {
				$scope: scope
			});
		}));

		/*it('$scope.find() should create array with available eventtypes', inject(function(Eventtypes) {

			var grade = new Eventtypes({
				name: 'Teacher adds grade for class'
			});

			var registration = new Eventtypes({
				name: 'Teacher adds book to course'
			});

			var eventtypes = [grade, registration];

			// Set GET response
			$httpBackend.expectGET('eventtypes').respond(eventtypes);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.eventtypes).toEqualData(eventtypes);
		}));*/

		it('$scope.findOne() should create an array with one Eventtype object fetched from XHR using a eventtypeId URL parameter', inject(function(Eventtypes) {
			// eventtype example
			var eventtype = new Eventtypes({
				name: 'Teacher adds book to course'
			});

			// Set the URL parameter
			$stateParams.eventtypeId = 1;

			// Set GET response
			$httpBackend.expectGET(/eventtypes\/([0-9]{1,})$/).respond(eventtype);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.eventtype).toEqualData(eventtype);
		}));
	});
}());