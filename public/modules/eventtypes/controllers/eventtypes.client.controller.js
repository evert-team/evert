'use strict';

// Eventtypes controller

angular.module('eventtypes').controller('EventtypesController', ['$scope', '$rootScope', '$stateParams', '$location', 'Authentication', 'Eventhandlers', 'Eventtypes',
    function($scope, $rootScope, $stateParams, $location, Authentication, Eventhandlers, Eventtypes) {
        $scope.authentication = Authentication;

        // By which attribute should Evert sort the handler list.
        $scope.setOrder = function(order) {
            $scope.order = order;
        };

        // Find a list of Eventtypes
        $scope.find = function() {
            $scope.eventtypes = Eventtypes.query();
            $scope.eventhandlers = Eventhandlers.query();

            var tempCounter = 0,
                tempEventIdList = [];

            $scope.eventtypes.$promise.then(function(types) {
                
                for (var i = 0; i < types.length; i++) {

                    tempEventIdList.push(types[i].eventId);
                }

                $scope.eventhandlers.$promise.then(function(handler) {

                    for (var j = 0; j < tempEventIdList.length; j++) {

                        for (var k = 0; k < handler.length; k++) {

                            if (handler[k].parentId === tempEventIdList[j]) {
                                tempCounter++;
                            }
                        }

                        $scope.eventtypes[j].noHandlers = tempCounter;
                        tempCounter = 0;
                    }
                });
            });
        };

        // Find existing Eventtype
        $scope.findOne = function() {
            $scope.eventtype = Eventtypes.get({
                eventtypeId: $stateParams.eventtypeId
            });
        };
    }
]);
