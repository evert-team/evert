'use strict';
// Init the application configuration module for AngularJS application
var ApplicationConfiguration = function () {
    // Init module configuration options
    var applicationModuleName = 'evert';
    var applicationModuleVendorDependencies = [
        'ngResource',
        'ngCookies',
        'ngAnimate',
        'ngTouch',
        'ngSanitize',
        'ui.router',
        'ui.bootstrap',
        'ui.utils',
        'ui.ace'
    ];

    // Add a new vertical module
    var registerModule = function (moduleName, dependencies) {
      // Create angular module
      angular.module(moduleName, dependencies || []);
      // Add the module to the AngularJS configuration file
      angular.module(applicationModuleName).requires.push(moduleName);
    };
    return {
      applicationModuleName: applicationModuleName,
      applicationModuleVendorDependencies: applicationModuleVendorDependencies,
      registerModule: registerModule
    };
  }();'use strict';
//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);
// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config([
  '$locationProvider',
  function ($locationProvider) {
    $locationProvider.hashPrefix('!');
  }
]);
//Then define the init function for starting up the application
angular.element(document).ready(function () {
  //Fixing facebook bug with redirect
  if (window.location.hash === '#_=_')
    window.location.hash = '#!';
  //Then init the app
  angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});'use strict';
// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('eventhandlers');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('eventtypes');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('scripts');'use strict';
// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('users');$(document).ready(function () {
});'use strict';
// Setting up route
angular.module('core').config([
  '$stateProvider',
  '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {
    // Redirect to home view when route not found
    $urlRouterProvider.otherwise('/');
    // Home state routing
    $stateProvider.state('help', {
      url: '/help',
      templateUrl: 'modules/core/views/help.client.view.html'
    }).state('home', {
      url: '/',
      templateUrl: 'modules/users/views/authentication/signin.client.view.html'
    });
  }
]);'use strict';
angular.module('core').controller('HeaderController', [
  '$scope',
  'Authentication',
  'Menus',
  function ($scope, Authentication, Menus) {
    $scope.authentication = Authentication;
    $scope.isCollapsed = false;
    $scope.menu = Menus.getMenu('topbar');
    $scope.toggleCollapsibleMenu = function () {
      $scope.isCollapsed = !$scope.isCollapsed;
    };
    // Collapsing the menu after navigation
    $scope.$on('$stateChangeSuccess', function () {
      $scope.isCollapsed = false;
    });
  }
]);'use strict';
angular.module('core').controller('HomeController', [
  '$scope',
  function ($scope) {
  }
]);'use strict';
//Menu service used for managing  menus
angular.module('core').service('Menus', [function () {
    // Define a set of default roles
    this.defaultRoles = ['*'];
    // Define the menus object
    this.menus = {};
    // A private function for rendering decision 
    var shouldRender = function (user) {
      if (user) {
        if (!!~this.roles.indexOf('*')) {
          return true;
        } else {
          for (var userRoleIndex in user.roles) {
            for (var roleIndex in this.roles) {
              if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
                return true;
              }
            }
          }
        }
      } else {
        return this.isPublic;
      }
      return false;
    };
    // Validate menu existance
    this.validateMenuExistance = function (menuId) {
      if (menuId && menuId.length) {
        if (this.menus[menuId]) {
          return true;
        } else {
          throw new Error('Menu does not exists');
        }
      } else {
        throw new Error('MenuId was not provided');
      }
      return false;
    };
    // Get the menu object by menu id
    this.getMenu = function (menuId) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Return the menu object
      return this.menus[menuId];
    };
    // Add new menu object by menu id
    this.addMenu = function (menuId, isPublic, roles) {
      // Create the new menu
      this.menus[menuId] = {
        isPublic: isPublic || false,
        roles: roles || this.defaultRoles,
        items: [],
        shouldRender: shouldRender
      };
      // Return the menu object
      return this.menus[menuId];
    };
    // Remove existing menu object by menu id
    this.removeMenu = function (menuId) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Return the menu object
      delete this.menus[menuId];
    };
    // Add menu item object
    this.addMenuItem = function (menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Push new menu item
      this.menus[menuId].items.push({
        title: menuItemTitle,
        link: menuItemURL,
        menuItemType: menuItemType || 'item',
        menuItemClass: menuItemType,
        uiRoute: menuItemUIRoute || '/' + menuItemURL,
        isPublic: isPublic === null || typeof isPublic === 'undefined' ? this.menus[menuId].isPublic : isPublic,
        roles: roles === null || typeof roles === 'undefined' ? this.menus[menuId].roles : roles,
        position: position || 0,
        items: [],
        shouldRender: shouldRender
      });
      // Return the menu object
      return this.menus[menuId];
    };
    // Add submenu item object
    this.addSubMenuItem = function (menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Search for menu item
      for (var itemIndex in this.menus[menuId].items) {
        if (this.menus[menuId].items[itemIndex].link === rootMenuItemURL) {
          // Push new submenu item
          this.menus[menuId].items[itemIndex].items.push({
            title: menuItemTitle,
            link: menuItemURL,
            uiRoute: menuItemUIRoute || '/' + menuItemURL,
            isPublic: isPublic === null || typeof isPublic === 'undefined' ? this.menus[menuId].items[itemIndex].isPublic : isPublic,
            roles: roles === null || typeof roles === 'undefined' ? this.menus[menuId].items[itemIndex].roles : roles,
            position: position || 0,
            shouldRender: shouldRender
          });
        }
      }
      // Return the menu object
      return this.menus[menuId];
    };
    // Remove existing menu object by menu id
    this.removeMenuItem = function (menuId, menuItemURL) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Search for menu item to remove
      for (var itemIndex in this.menus[menuId].items) {
        if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
          this.menus[menuId].items.splice(itemIndex, 1);
        }
      }
      // Return the menu object
      return this.menus[menuId];
    };
    // Remove existing menu object by menu id
    this.removeSubMenuItem = function (menuId, submenuItemURL) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Search for menu item to remove
      for (var itemIndex in this.menus[menuId].items) {
        for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
          if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
            this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
          }
        }
      }
      // Return the menu object
      return this.menus[menuId];
    };
    //Adding the topbar menu
    this.addMenu('topbar');
  }]);'use strict';
//Setting up route
angular.module('eventhandlers').config([
  '$stateProvider',
  function ($stateProvider) {
    // Eventhandlers state routing
    $stateProvider.state('eventtype.viewEventtype.handler', {
      url: '/eventhandler/:eventhandlerId',
      templateUrl: 'modules/eventhandlers/views/view-eventhandler.client.view.html'
    });
  }
]);'use strict';
angular.module('eventhandlers').controller('CreateModalController', [
  '$scope',
  '$modalInstance',
  'description',
  function ($scope, $modalInstance, description) {
    $scope.type = description;
    $scope.ok = function (data) {
      $modalInstance.close(data);
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }
]);'use strict';
angular.module('eventhandlers').controller('DeleteAllController', [
  '$scope',
  '$modalInstance',
  'eventhandlers',
  function ($scope, $modalInstance, eventhandlers) {
    $scope.ok = function () {
      $modalInstance.close(eventhandlers);
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }
]);'use strict';
angular.module('eventhandlers').controller('ModalInstanceCtrl', [
  '$scope',
  '$modalInstance',
  'handler',
  function ($scope, $modalInstance, handler) {
    $scope.handler = handler.description;
    $scope.ok = function () {
      $modalInstance.close(handler);
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }
]);'use strict';
// Eventhandlers controller
angular.module('eventhandlers').controller('EventhandlersController', [
  '$scope',
  '$rootScope',
  '$stateParams',
  '$location',
  'Authentication',
  'Eventhandlers',
  '$modal',
  '$log',
  '$state',
  '$timeout',
  '$http',
  function ($scope, $rootScope, $stateParams, $location, Authentication, Eventhandlers, $modal, $log, $state, $timeout, $http) {
    $scope.authentication = Authentication;
    var example = [
        '//Example of using others systems API to get',
        '//further information to use in your own script,',
        '//for example in emails, text messages, etc.',
        '//This example uses the information to send an email',
        '/*',
        ' * //Set the email address and subject to your liking.',
        ' * var address = "Add email address";',
        ' * var subject = "Add subject;"',
        ' *',
        ' * //Get information from Centris, the client API.',
        ' * //This information is only available within,',
        ' * //the following function.',
        ' * client.get("/api/api/v1/courses/?department=1", function (data) {',
        ' *    for ( var i in data) {',
        ' *        if( data[i].Name === "{{course.name}}") {',
        ' *            var mainContent = "The book {{book.name}} has been registered',
        ' *            to {{course.name}} in {{department.name}}.',
        ' *            There are  " + data[i].RegisteredStudents +',
        ' *            " students in this course this semester";',
        ' *            sendEmail(address, subject, mainContent);',
        ' *        }',
        ' *    }',
        ' * });',
        ' */'
      ].join('\n');
    $scope.placeholder = {};
    $scope.client = {};
    $scope.evert = {};
    $scope.query = {};
    // Adding information from selected button to template
    $scope.addInfo = function (data, type) {
      if ($scope.test === false) {
        $scope.test = true;
      } else {
        $scope.test = false;
      }
      if (type === 'info') {
        $scope.infoToAdd = '{{' + data + '}}';
      } else if (type === 'client') {
        $scope.infoToAdd = [
          'client.get("' + data + '", function (data) {',
          '    console.log(data);',
          '});'
        ].join('\n');
      } else if (type === 'evert') {
        $scope.infoToAdd = data;
      } else if (type === 'api') {
        $scope.infoToAdd = [
          'var path = "Add path"',
          'var host = "Add host"',
          'api.get(path, host, function(data) {',
          '    console.log(data);',
          '});'
        ].join('\n');
      }
      $rootScope.$broadcast('addInfo', $scope.infoToAdd, $scope.eventhandler);
    };
    // Create new Eventhandler
    $scope.create = function (type, description, id) {
      // Create new Eventhandler object
      var eventhandler = new Eventhandlers({
          name: type,
          parentId: $stateParams.eventtypeId,
          description: description,
          content: { message: '' }
        });
      if (type === 'js') {
        console.log(eventhandler);
        eventhandler.content.message = example;
      }
      // Redirect after save
      eventhandler.$save(function (response) {
        $location.path('eventtype/' + response.parentId + '/eventhandler/' + response._id);
        $scope.eventhandlers.push(response);
        // Clear form fields
        $scope.name = '';
        $scope.description = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Remove existing Eventhandler
    var remove = function (eventhandler) {
      // Change view if viewing deleted handler
      if (eventhandler._id === $stateParams.eventhandlerId) {
        $location.path('eventtype/' + eventhandler.parentId);
      }
      // Remove from handler list
      if (eventhandler) {
        eventhandler.$remove();
        for (var i in $scope.eventhandlers) {
          if ($scope.eventhandlers[i] === eventhandler) {
            $scope.eventhandlers.splice(i, 1);
          }
        }
      } else {
        $scope.eventhandler.$remove(function () {
          $location.path('eventhandlers');
        });
      }
    };
    // Deleting all available eventhandlers for this type
    var removeAll = function () {
      while ($scope.eventhandlers.length !== 0) {
        remove($scope.eventhandlers[0]);
      }
    };
    // Update existing Eventhandler
    $scope.update = function (response, putResponseHeaders) {
      var eventhandler = $scope.eventhandler;
      var id = $stateParams.eventtypeId;
      eventhandler.$update(function (response) {
        response.$promise.then(function () {
          $scope.saved = true;
          $timeout(function () {
            $scope.saved = false;
          }, 2000);
        });
        $location.path('eventtype/' + id + '/eventhandler/' + eventhandler._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Find a list of Eventhandlers
    $scope.find = function () {
      $scope.showAll = false;
      $scope.id = parseInt($stateParams.eventtypeId, 10);
      // information for checkbox
      if ($scope.showAll)
        $scope.info = 'Active';
      else
        $scope.info = 'Show all';
      var eventhandlers = Eventhandlers.query();
      eventhandlers.$promise.then(function (handler) {
        $scope.eventhandlers = handler;
      });
    };
    // Find existing Eventhandler
    $scope.findOne = function () {
      $scope.eventhandler = Eventhandlers.get({ eventhandlerId: $stateParams.eventhandlerId });
    };
    // Confirmation modal for deleting  handling
    $scope.deleteConfirm = function (handlers) {
      $scope.handlers = handlers;
      var modalInstance = $modal.open({
          templateUrl: 'delete.html',
          controller: 'ModalInstanceCtrl',
          resolve: {
            handler: function () {
              return handlers;
            }
          }
        });
      modalInstance.result.then(function (handler) {
        remove(handler);
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };
    $scope.isActive = function (handler) {
      console.log(handler);
      handler.$update(function (response) {
        console.log(response);
        response.$promise.then(function () {
          $timeout(function () {
            $scope.saved = false;
          }, 2000);
        });
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Confirmation modal for deleting all handlings for this type
    $scope.deleteAllConfirm = function () {
      var modalInstance = $modal.open({
          templateUrl: 'deleteAll.html',
          controller: 'DeleteAllController',
          resolve: {
            eventhandlers: function () {
              return $scope.eventhandlers;
            }
          }
        });
      modalInstance.result.then(function (eventhandlers) {
        removeAll(eventhandlers);
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };
    // Create modal to create new handling
    $scope.createHandler = function (type) {
      var createModal = $modal.open({
          templateUrl: 'createModal.html',
          controller: 'CreateModalController',
          resolve: {
            description: function () {
              return type;
            }
          }
        });
      createModal.result.then(function (description) {
        $scope.create(type, description);
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };
    //Available evert functions
    $scope.evertFunctions = function () {
      $scope.functions = [
        {
          name: 'Email',
          value: [
            'var address = "Add email address";',
            'var subject = "Add subject;"',
            'var mainContent = "Add content";',
            'sendEmail(address, subject, mainContent);'
          ].join('\n')
        },
        {
          name: 'Text message',
          value: [
            'var phoneNumber = "Add phone number";',
            'var mainContent = "Add content";',
            'sendSMS(phoneNumber, mainContent)'
          ].join('\n')
        }
      ];
    };
  }
]);'use strict';
angular.module('eventtypes').directive('information', [
  '$rootScope',
  function ($rootScope) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        $rootScope.$on('addInfo', function (e, val, handler) {
          var template = element[0];
          var startPos = template.selectionStart;
          var endPos = template.selectionEnd;
          var scrollTop = template.scrollTop;
          template.value = template.value.substring(0, startPos) + val + template.value.substring(endPos, template.value.length);
          template.focus();
          template.selectionStart = startPos + val.length;
          template.selectionEnd = startPos + val.length;
          template.scrollTop = scrollTop;
          handler.content.message = template.value;
        });
      }
    };
  }
]);'use strict';
angular.module('eventhandlers').filter('handlerList', [function () {
    return function (handlers, id, isActive) {
      var result = [], i = 0;
      // If checkbox checked then filter out disabled handlers
      if (isActive) {
        for (i in handlers) {
          if (handlers[i].active === true && handlers[i].parentId === id) {
            result.push(handlers[i]);
          }
        }
      } else {
        for (i in handlers) {
          if (handlers[i].parentId === id) {
            result.push(handlers[i]);
          }
        }
      }
      return result;
    };
  }]);'use strict';
//Eventhandlers service used to communicate Eventhandlers REST endpoints
angular.module('eventhandlers').factory('Eventhandlers', [
  '$resource',
  function ($resource) {
    return $resource('eventhandler/:eventhandlerId', { eventhandlerId: '@_id' }, { update: { method: 'PUT' } });
  }
]);'use strict';
// Configuring the Articles module
angular.module('eventtypes').run([
  'Menus',
  function (Menus) {
    // Set top bar menu items
    Menus.addMenuItem('topbar', 'Eventtypes', 'eventtypes', 'dropdown');
    Menus.addSubMenuItem('topbar', 'eventtypes', 'List Eventtypes', 'eventtypes');
    Menus.addSubMenuItem('topbar', 'eventtypes', 'New Eventtype', 'eventtypes/create');
  }
]);'use strict';
//Setting up route
angular.module('eventtypes').config([
  '$stateProvider',
  '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {
    // Eventtypes state routing
    $stateProvider.state('eventtype', {
      url: '/eventtype',
      templateUrl: '/modules/eventtypes/views/eventtype.client.view.html'
    }).state('eventtype.viewEventtype', {
      url: '/:eventtypeId',
      views: {
        'name': { templateUrl: 'modules/eventtypes/views/name-eventtype.client.view.html' },
        'handlerView': { templateUrl: 'modules/eventhandlers/views/eventhandler.client.view.html' },
        'handlerList': {
          url: '',
          templateUrl: 'modules/eventhandlers/views/list-eventhandlers.client.view.html'
        }
      }
    }).state('eventtype.viewEventtype.handler.buttons', {
      url: '',
      templateUrl: 'modules/eventtypes/views/view-eventtype.client.view.html'
    }).state('listEventtypes', {
      url: '/eventtypes',
      templateUrl: 'modules/eventtypes/views/list-eventtypes.client.view.html'
    });
  }
]);'use strict';
// Eventtypes controller
angular.module('eventtypes').controller('EventtypesController', [
  '$scope',
  '$rootScope',
  '$stateParams',
  '$location',
  'Authentication',
  'Eventhandlers',
  'Eventtypes',
  function ($scope, $rootScope, $stateParams, $location, Authentication, Eventhandlers, Eventtypes) {
    $scope.authentication = Authentication;
    // By which attribute should Evert sort the handler list.
    $scope.setOrder = function (order) {
      $scope.order = order;
    };
    // Find a list of Eventtypes
    $scope.find = function () {
      $scope.eventtypes = Eventtypes.query();
      $scope.eventhandlers = Eventhandlers.query();
      var tempCounter = 0, tempEventIdList = [];
      $scope.eventtypes.$promise.then(function (types) {
        for (var i = 0; i < types.length; i++) {
          tempEventIdList.push(types[i].eventId);
        }
        $scope.eventhandlers.$promise.then(function (handler) {
          for (var j = 0; j < tempEventIdList.length; j++) {
            for (var k = 0; k < handler.length; k++) {
              if (handler[k].parentId === tempEventIdList[j]) {
                tempCounter++;
              }
            }
            // console.log('type: '+tempEventIdList[j] + ' / has: ' + tempCounter)
            $scope.eventtypes[j].noHandlers = tempCounter;
            // console.log($scope.eventtypes)
            tempCounter = 0;
          }
        });
      });
    };
    // Find existing Eventtype
    $scope.findOne = function () {
      $scope.eventtype = Eventtypes.get({ eventtypeId: $stateParams.eventtypeId });
    };
  }
]);'use strict';
//Eventtypes service used to communicate Eventtypes REST endpoints
angular.module('eventtypes').factory('Eventtypes', [
  '$resource',
  function ($resource) {
    return $resource('eventtypes/:eventtypeId', { eventtypeId: '@eventId' }, { update: { method: 'PUT' } });
  }
]);'use strict';
//Setting up route
angular.module('scripts').config([
  '$stateProvider',
  function ($stateProvider) {
    // Scripts state routing
    $stateProvider.state('listScripts', {
      url: '/scripts',
      templateUrl: 'modules/scripts/views/list-scripts.client.view.html'
    }).state('createScript', {
      url: '/scripts/create',
      templateUrl: 'modules/scripts/views/create-script.client.view.html'
    }).state('viewScript', {
      url: '/scripts/:scriptId',
      templateUrl: 'modules/scripts/views/view-script.client.view.html'
    }).state('editScript', {
      url: '/scripts/:scriptId/edit',
      templateUrl: 'modules/scripts/views/edit-script.client.view.html'
    });
  }
]);'use strict';
// Scripts controller
angular.module('scripts').controller('ScriptsController', [
  '$scope',
  '$stateParams',
  '$location',
  'Authentication',
  'Scripts',
  function ($scope, $stateParams, $location, Authentication, Scripts) {
    $scope.authentication = Authentication;
    // Find a list of Scripts
    $scope.find = function () {
      $scope.scripts = Scripts.query();
    };  //**************Can this be deleted?********************************
        // Find existing Script
        //$scope.findOne = function() {
        //	$scope.script = Scripts.get({
        //		scriptId: $stateParams.scriptId
        //	});
        //};
  }
]);'use strict';
//Scripts service used to communicate Scripts REST endpoints
angular.module('scripts').factory('Scripts', [
  '$resource',
  function ($resource) {
    return $resource('scripts/:scriptId', { scriptId: '@_id' }, { update: { method: 'PUT' } });
  }
]);'use strict';
// Config HTTP Error Handling
angular.module('users').config([
  '$httpProvider',
  function ($httpProvider) {
    // Set the httpProvider "not authorized" interceptor
    $httpProvider.interceptors.push([
      '$q',
      '$location',
      'Authentication',
      function ($q, $location, Authentication) {
        return {
          responseError: function (rejection) {
            switch (rejection.status) {
            case 401:
              // Deauthenticate the global user
              Authentication.user = null;
              // Redirect to signin page
              $location.path('signin');
              break;
            case 403:
              // Add unauthorized behaviour 
              break;
            }
            return $q.reject(rejection);
          }
        };
      }
    ]);
  }
]);'use strict';
// Setting up route
angular.module('users').config([
  '$stateProvider',
  function ($stateProvider) {
    // Users state routing
    $stateProvider.state('profile', {
      url: '/settings/profile',
      templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
    }).state('password', {
      url: '/settings/password',
      templateUrl: 'modules/users/views/settings/change-password.client.view.html'
    }).state('accounts', {
      url: '/settings/accounts',
      templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
    }).state('signup', {
      url: '/signup',
      templateUrl: 'modules/users/views/authentication/signup.client.view.html'
    }).state('signin', {
      url: '/signin',
      templateUrl: 'modules/users/views/authentication/signin.client.view.html'
    }).state('forgot', {
      url: '/password/forgot',
      templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
    }).state('reset-invlaid', {
      url: '/password/reset/invalid',
      templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
    }).state('reset-success', {
      url: '/password/reset/success',
      templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
    }).state('reset', {
      url: '/password/reset/:token',
      templateUrl: 'modules/users/views/password/reset-password.client.view.html'
    });
  }
]);'use strict';
angular.module('users').controller('AuthenticationController', [
  '$scope',
  '$http',
  '$location',
  'Authentication',
  function ($scope, $http, $location, Authentication) {
    $scope.authentication = Authentication;
    // If user is signed in then redirect back home
    if ($scope.authentication.user)
      $location.path('/eventtypes');
    $scope.signup = function () {
      $http.post('/auth/signup', $scope.credentials).success(function (response) {
        // If successful we assign the response to the global user model
        $scope.authentication.user = response;
        // And redirect to the index page
        $location.path('/');
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
    $scope.signin = function () {
      $http.post('/auth/signin', $scope.credentials).success(function (response) {
        // If successful we assign the response to the global user model
        $scope.authentication.user = response;
        // And redirect to the event types list
        $location.path('/eventtypes');
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);'use strict';
angular.module('users').controller('PasswordController', [
  '$scope',
  '$stateParams',
  '$http',
  '$location',
  'Authentication',
  function ($scope, $stateParams, $http, $location, Authentication) {
    $scope.authentication = Authentication;
    //If user is signed in then redirect back home
    if ($scope.authentication.user)
      $location.path('/');
    // Submit forgotten password account id
    $scope.askForPasswordReset = function () {
      $scope.success = $scope.error = null;
      $http.post('/auth/forgot', $scope.credentials).success(function (response) {
        // Show user success message and clear form
        $scope.credentials = null;
        $scope.success = response.message;
      }).error(function (response) {
        // Show user error message and clear form
        $scope.credentials = null;
        $scope.error = response.message;
      });
    };
    // Change user password
    $scope.resetUserPassword = function () {
      $scope.success = $scope.error = null;
      $http.post('/auth/reset/' + $stateParams.token, $scope.passwordDetails).success(function (response) {
        // If successful show success message and clear form
        $scope.passwordDetails = null;
        // Attach user profile
        Authentication.user = response;
        // And redirect to the index page
        $location.path('/password/reset/success');
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);'use strict';
angular.module('users').controller('SettingsController', [
  '$scope',
  '$http',
  '$location',
  'Users',
  'Authentication',
  function ($scope, $http, $location, Users, Authentication) {
    $scope.user = Authentication.user;
    // If user is not signed in then redirect back home
    if (!$scope.user)
      $location.path('/');
    // Check if there are additional accounts 
    $scope.hasConnectedAdditionalSocialAccounts = function (provider) {
      for (var i in $scope.user.additionalProvidersData) {
        return true;
      }
      return false;
    };
    // Check if provider is already in use with current user
    $scope.isConnectedSocialAccount = function (provider) {
      return $scope.user.provider === provider || $scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider];
    };
    // Remove a user social account
    $scope.removeUserSocialAccount = function (provider) {
      $scope.success = $scope.error = null;
      $http.delete('/users/accounts', { params: { provider: provider } }).success(function (response) {
        // If successful show success message and clear form
        $scope.success = true;
        $scope.user = Authentication.user = response;
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
    // Update a user profile
    $scope.updateUserProfile = function (isValid) {
      if (isValid) {
        $scope.success = $scope.error = null;
        var user = new Users($scope.user);
        user.$update(function (response) {
          $scope.success = true;
          Authentication.user = response;
        }, function (response) {
          $scope.error = response.data.message;
        });
      } else {
        $scope.submitted = true;
      }
    };
    // Change user password
    $scope.changeUserPassword = function () {
      $scope.success = $scope.error = null;
      $http.post('/users/password', $scope.passwordDetails).success(function (response) {
        // If successful show success message and clear form
        $scope.success = true;
        $scope.passwordDetails = null;
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);'use strict';
// Authentication service for user variables
angular.module('users').factory('Authentication', [function () {
    var _this = this;
    _this._data = { user: window.user };
    return _this._data;
  }]);'use strict';
// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', [
  '$resource',
  function ($resource) {
    return $resource('users', {}, { update: { method: 'PUT' } });
  }
]);