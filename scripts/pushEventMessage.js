#!/usr/bin/env node

var amqp = require('amqplib');
var when = require('when');

amqp.connect('amqp://localhost').then(function(conn) {
  return when(conn.createChannel().then(function(ch) {
    
    //the queue name..
    var q = 'Evert';

    //the message from centris...
    var msg = '{"type":"event","typeId":"3","date":"20/09/2014","_comment":"This should have dd/mm/yyyy as a format","book":{"name":"Problem Solving with C++","isbn":"978-0132162739"},"course":{"numberofstudents":"378","name":"Forritun","instanceID":"24761","stardate":"18/08/2014","department":"Tölvunarfræðideild"}}';

    var ok = ch.assertQueue(q, {durable: false});
    
    return ok.then(function(_qok) {
      ch.sendToQueue(q, new Buffer(msg));



      //logga út á terminal message.
      console.log("Message sent to RabbitMQ on the Evert queue: " + msg);



      return ch.close();
    });
  })).ensure(function() { conn.close(); });;
}).then(null, console.warn);
