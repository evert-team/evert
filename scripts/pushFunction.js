#!/usr/bin/env node

var amqp = require('amqplib');
var when = require('when');

amqp.connect('amqp://localhost').then(function(conn) {
  return when(conn.createChannel().then(function(ch) {
    
    //the queue name..
    var q = 'Evert';

    //the message from centris...
    var msg = '{"type":"functions","user":"54410970ffd7b2601c0d471d","functions":[{"name":"Upplýsingar um kennara","value":"/api/api/v1/teachers","comment":"Returns all persons which have taught anything at the school."}]}';

    var ok = ch.assertQueue(q, {durable: false});
    
    return ok.then(function(_qok) { 
      ch.sendToQueue(q, new Buffer(msg));

      //logga út á terminal message.
      console.log("Message sent to RabbitMQ on the Evert queue: " + msg);



      return ch.close();
    });
  })).ensure(function() { conn.close(); });;
}).then(null, console.warn);

