#!/usr/bin/env node

var amqp = require('amqplib');
var when = require('when');

amqp.connect('amqp://admin:ruleit2012@localhost:5672').then(function(conn) {
  return when(conn.createChannel().then(function(ch) {
    
    //the queue name..
    var q = 'Evert';

    //the message from centris...
    var msg = '{"type":"new","user":"548c4bb3863ed53b7d142a72","id":"8","name":"Meeting Coming Up","comment":"1 is for an active event 0 is to deactivate the event.","isActive":"1","buttons":[{"value":"Date Now"},{"name":"department.name","value":"Department Name"},{"name":"meeting.time","value":"Time of Meeting"},{"name":"meeting.location","value":"Meeting Location"}]}';

    var ok = ch.assertQueue(q, {durable: false});
    
    return ok.then(function(_qok) { 
      ch.sendToQueue(q, new Buffer(msg));



      //logga út á terminal message.
      console.log("Message sent to RabbitMQ on the Evert queue: " + msg);



      return ch.close();
    });
  })).ensure(function() { conn.close(); });;
}).then(null, console.warn);
