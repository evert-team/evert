#!/usr/bin/env node

var amqp = require('amqplib');
var when = require('when');

amqp.connect('amqp://admin:ruleit2012@localhost:5672').then(function(conn) {
  return when(conn.createChannel().then(function(ch) {
    
    //the queue name..
    var q = 'Evert';

    //the message from centris...
    var msg = '{"type":"new","user":"548c4bb3863ed53b7d142a72","id":"1","name":"Book Registration for Course","comment":"1 is for an active event 0 is to deactivate the event.","isActive":"1","functions":[{"name":"student.number"}],"buttons":[{"value":"Date Now"},{"name":"book.name","value":"Book Name"},{"name":"book.isbn","value":"ISBN Number"},{"name":"course.numberofstudents","value":"Number of stundents"},{"name":"course.name","value":"Course Name"},{"name":"course.startdate","value":"Course startdate"},{"name":"course.department","value":"Course Department"}]}';

    var ok = ch.assertQueue(q, {durable: false});
    
    return ok.then(function(_qok) { 
      ch.sendToQueue(q, new Buffer(msg));



      //logga út á terminal message.
      console.log("Message sent to RabbitMQ on the Evert queue: " + msg);



      return ch.close();
    });
  })).ensure(function() { conn.close(); });;
}).then(null, console.warn);
