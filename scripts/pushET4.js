#!/usr/bin/env node

var amqp = require('amqplib');
var when = require('when');

amqp.connect('amqp://localhost').then(function(conn) {
  return when(conn.createChannel().then(function(ch) {
    
    //the queue name..
    var q = 'Evert';

    //the message from centris...
    var msg = '{"type":"new","user":"54410970ffd7b2601c0d471d","id":"5","name":"New grade has been published test","comment":"1 is for an active event 0 is to deactivate the event.","isActive":"1","functions":[{"name":"student.number"}],"buttons":[{"value":"Date Now"},{"name":"course.numberofstudents","value":"Number of stundents"},{"name":"course.name","value":"Course Name"},{"name":"course.department","value":"Course Department"},{"name":"graded.item","value":"Graded Item"}]}';

    var ok = ch.assertQueue(q, {durable: false});
    
    return ok.then(function(_qok) { 
      ch.sendToQueue(q, new Buffer(msg));



      //logga út á terminal message.
      console.log("Message sent to RabbitMQ on the Evert queue: " + msg);



      return ch.close();
    });
  })).ensure(function() { conn.close(); });;
}).then(null, console.warn);
