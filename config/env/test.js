'use strict';

module.exports = {
	db: 'mongodb://Evert:teamEvert@linus.mongohq.com:10043/TestEvert',
	port: 3001,
	app: {
		title: 'Evert - Test Environment'
	},
	facebook: {
		clientID: process.env.FACEBOOK_ID || 'APP_ID',
		clientSecret: process.env.FACEBOOK_SECRET || 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/facebook/callback'
	},
	twitter: {
		clientID: process.env.TWITTER_KEY || 'CONSUMER_KEY',
		clientSecret: process.env.TWITTER_SECRET || 'CONSUMER_SECRET',
		callbackURL: 'http://localhost:3000/auth/twitter/callback'
	},
	google: {
		clientID: process.env.GOOGLE_ID || 'APP_ID',
		clientSecret: process.env.GOOGLE_SECRET || 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/google/callback'
	},
	linkedin: {
		clientID: process.env.LINKEDIN_ID || 'APP_ID',
		clientSecret: process.env.LINKEDIN_SECRET || 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/linkedin/callback'
	},
	github: {
		clientID: process.env.GITHUB_ID || 'APP_ID',
		clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/github/callback'
	},
	rabbit: {
		url: 'amqp://localhost',
		queue: 'Evert'
	},
	sms: {
		user: 'teamevert',
		pass: 'cstnmsitopjbuf',
		countryCode: '354',
		apiID: '3513888',
		path: 'todo'
	},
	clientApi: {
		host: '10.17.3.26'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'Evert <projectevert@gmail.com>',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'Gmail',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'projectevert@gmail.com',
				pass: process.env.MAILER_PASSWORD || 'teamEvert'
			}
		}
	}
};